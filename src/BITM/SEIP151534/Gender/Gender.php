<?php

namespace App\Gender;
use App\Message\Message;
use App\model\database as db;
use App\Utility\Utility;

//require_once("../../../../vendor/autoload.php");




class Gender extends db{
    public $id;
    public $name;
    public $sex;
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data = Null)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];

        }
        if (array_key_exists('name', $data)) {
            $this->name= $data['name'];

        }
        if (array_key_exists('sex', $data)) {
            $this->sex= $data['sex'];

        }

    }
    public function store(){
        $arrData=array($this->name,$this->sex);

        $sql= "Insert INTO gender(name,sex ) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("Sucess!data has been inserted sucessfully");
        else
            Message::setMessage("Failure!data has not been inserted sucessfully");
        Utility::redirect('create.php');
    }// end of store method
}
//$objGender=new Gender();

