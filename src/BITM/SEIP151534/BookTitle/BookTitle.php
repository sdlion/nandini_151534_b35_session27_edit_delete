<?php
namespace App\BookTitle;

use App\Message\Message;
use App\Model\database as db;
use App\Utility\Utility;
Use PDO;
//require_once("../../../../vendor/autoload.php");
class BookTitle extends db
{
    public $id;
    public $book_title;
    public $author_name;

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data = Null)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];

        }
        if (array_key_exists('book_title', $data)) {
            $this->book_title = $data['book_title'];

        }
        if (array_key_exists('author_name', $data)) {
            $this->author_name = $data['author_name'];

        }//end of set data();

    }

    public function store(){
        $arrData=array($this->book_title,$this->author_name);

        $sql= "Insert INTO book_title(book_title, author_name) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

if($result)
    Message::setMessage("Sucess!data has been inserted sucessfully");
        else
            Message::setMessage("Failure!data has not been inserted sucessfully");
        Utility::redirect('create.php');
    }// end of store method

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from book_title');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();
    public function view($fetchMode='ASSOC'){
$sql='SELECT * from book_title WHERE id='.$this->id;
        $STH = $this->DBH->query($sql);
        //echo $sql;


        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function update(){
        $arrData=array($this->book_title,$this->author_name);

        //UPDATE `atomic_project_35`.`book_title` SET `book_title` = 'nan' WHERE `book_title`.`id` = 4;

        $sql="update book_title SET  book_title=? ,author_name=? WHERE id=".$this->id;
        $STH =$this->DBH->prepare($sql);
        $STH->execute($arrData);
Utility::redirect('index.php');
    }//end of update();
    public function delete(){
        $sql="Delete from book_title where id=".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');

    }
}




//public function index(){

    //$STH = $this->DBH->query('SELECT * from book_title ORDER BY book_title ASC ');
    //$STH->setFetchMode(PDO::FETCH_OBJ);
    //$arrAllData=$STH->fetchall();
    //return $arrAllData;






































































































